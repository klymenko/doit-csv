* Prerequisites
    node 13.9.0
    npm 6.13.7
    mongodb 3.6.8
* Build:
 npm install        
* Test:
 npm test
* Run:
 npm start

* Usage:
 1. configure all of the connection details(database/mailing) using config/default.json
 2. Start the app.
 3. Enter location of patients.csv file.