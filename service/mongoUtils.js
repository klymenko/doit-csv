const MongoClient = require('mongodb').MongoClient;
const config = require('config');

async function dropCollection(collectionName) {
    return new Promise(
    function (resolve, reject) {
            MongoClient.connect(config.get('mongodb.uri'), function(err, client) {
                const db = client.db(config.get('mongodb.dbName'));
                db.collection(collectionName).drop();
                client.close();
                resolve("ok");
            });
        });

}

exports.dropCollection = dropCollection;