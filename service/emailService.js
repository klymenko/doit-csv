const MongoClient = require('mongodb').MongoClient;
const config = require('config');
const mongoUtils = require('./mongoUtils');
const schedule = require('node-schedule');
const nodemailer = require('nodemailer');

const dayInMillis = 60*60*24*1000;


let testEmailAccount = nodemailer.createTestAccount();

let transporter = nodemailer.createTransport({
    host: config.get('emailing.host'),
    port: 587,
    secure: false,
    auth: {
        user: testEmailAccount.user,
        pass: testEmailAccount.pass
    }
});

async function generateEmailTemplates() {
    return new Promise(
        function (resolve, reject) {
            mongoUtils.dropCollection(config.get('emailing.emailsCollectionName')).then(res => {
                MongoClient.connect(config.get('mongodb.uri'), function(err, client) {
                    const db = client.db(config.get('mongodb.dbName'));
                    const collection = db.collection(config.get('emailing.emailsCollectionName'));
                    const rows = [];
                    for(i = config.get('emailing.generationStartIndx'); i < config.get('emailing.generationEndIndx'); i++) {
                        const emailingDate = new Date((new Date()).getTime() + (dayInMillis * i));
                        rows.push({
                            'id': i + 1,
                            'name': 'Day ' + (i + 1),
                            'scheduled_date': emailingDate,
                            'text': 'Some useful text',
                            'sent': false
                        });
                    }
                    collection.insertMany(
                        rows,
                        function(err, result) {
                            if(err) {
                                reject("");
                            } else {
                                resolve("ok");
                            }
                        }
                    );
                    client.close();

                });
            });
    });
}

function scheduleEmailsJob() {
    schedule.scheduleJob(config.get('emailing.schedule'), () => selectEmailsToSend());
}
function selectEmailsToSend() {
    MongoClient.connect(config.get('mongodb.uri'), function(err, client) {
        const db = client.db(config.get('mongodb.dbName'));
        const collection = db.collection(config.get('emailing.emailsCollectionName'));
        collection.find({
            'sent': false,
            'scheduled_date' : {'$lt': new Date()}
        }).toArray(function(err, docs){
            docs.map(it => it['text']).forEach(processEmailToSend);
            docs.forEach(it => {
                updateSentEmails(it['id']);
            });
        });
        client.close();
    });
}
function updateSentEmails(id) {
    MongoClient.connect(config.get('mongodb.uri'), function(err, client) {
        const db = client.db(config.get('mongodb.dbName'));
        const collection = db.collection(config.get('emailing.emailsCollectionName'));
        collection.updateOne(
            {'id': id},
            {'$set': { 'sent': true }
        });
    });
}
function processEmailToSend(emailTxt, index) {
    sendEmailToPatients(emailTxt);
}
function sendEmailToPatients(emailTxt) {
    MongoClient.connect(config.get('mongodb.uri'), function(err, client) {
        const db = client.db(config.get('mongodb.dbName'));
        const collection = db.collection(config.get('emailing.patientsCollection'));
        collection.find({'CONSENT': 'Y'}).toArray(function(err, docs){
            sendEmailToPatient(emailTxt, docs.map(it => it['Email Address']));
        });
        client.close();
    });
}
function sendEmailToPatient(emailTxt, patientEmails) {
    transporter.sendMail({
        from: config.get('emailing.from'),
        to: patientEmails.join(),
        subject: config.get('emailing.subject'),
        text: emailTxt
    });
}


exports.generateEmailTemplates = generateEmailTemplates;
exports.scheduleEmailsJob = scheduleEmailsJob;