const fs = require('fs');
const csv = require('fast-csv');

const config = require('config');

const mongoUtils = require('./mongoUtils');

const MongoClient = require('mongodb').MongoClient;

const fileExtension = '.csv';
const delimiter = '|';
const encoding = 'UTF-8'

async function readAndPersistCsv(filePath, collectionName) {
    return new Promise(
        function (resolve, reject) {
            const data = fs.readFileSync(filePath, encoding);
            const lines = data.split(/\n/);
            if(lines.length > 0) {
                mongoUtils.dropCollection(collectionName).then(res => {
                    var columnNames = [];
                    MongoClient.connect(config.get('mongodb.uri'), function(err, client) {
                        const parsedRows = [];
                        lines.forEach((line) => {
                            if(columnNames.length == 0) {
                                columnNames = line.split(delimiter);
                            } else {
                                const parsedRow = {};
                                const rowValues = line.split(delimiter);
                                for(i = 0; i < columnNames.length; i++) {
                                    parsedRow[columnNames[i]] = rowValues[i];
                                }
                                parsedRows.push(parsedRow);
                            }
                        });
                        const db = client.db(config.get('mongodb.dbName'));
                        const collection = db.collection(collectionName);
                        collection.insertMany(
                            parsedRows,
                            function(err, result) {
                                client.close();
                                if(err) {
                                    reject(err);
                                } else {
                                    resolve("ok");
                                }
                            }
                        )
                    });
                });
            }
        }
    );
}

exports.readAndPersistCsv = readAndPersistCsv;