const assert = require('assert');
const csvParserService = require('../service/csvParserService');
const emailService = require('../service/emailService');
const MongoClient = require('mongodb').MongoClient;
const fs = require('fs');
const config = require('config');
const filePath = 'patients.csv';

describe('File-collection data match', () => {
    it('Csv content and collection data should be equal', () => {
            csvParserService.readAndPersistCsv(filePath, 'test1').then(result => {
            const data = fs.readFileSync(filePath, 'UTF-8');
            const lines = data.split(/\n/);
            const parsedRows = [];
            var columnNames = [];
            lines.forEach((line) => {
                if(columnNames.length == 0) {
                    columnNames = line.split('|');
                } else {
                    const parsedRow = {};
                    const rowValues = line.split('|');
                    for(i = 0; i < columnNames.length; i++) {
                        parsedRow[columnNames[i]] = rowValues[i];
                    }
                    parsedRows.push(parsedRow);
                }
            });
            MongoClient.connect(config.get('mongodb.uri'), function(err, client) {
                const db = client.db(config.get('mongodb.dbName'));
                const collection = db.collection('Patients');

                collection.find({}).toArray(function(err, docs){
                    docs.forEach((doc, indx) => {
                        delete doc._id;
                        assert.equal(JSON.stringify(doc), JSON.stringify(parsedRows[indx]));
                    });
                    client.close();
                });
            });
        });
    });




    it('Missing first name', () => {
        const expectedList = [15245, 16345];
        csvParserService.readAndPersistCsv(filePath, 'test2').then(result => {
            MongoClient.connect(config.get('mongodb.uri'), function(err, client) {
                const db = client.db(config.get('mongodb.dbName'));
                const collection = db.collection('Patients');
                collection.find({"First Name": ""}).toArray(function(err, docs) {
                    docs.forEach((it, indx) => {
                        assert.equal(it["Member ID"], expectedList[indx]);
                    });
                });
                assert.equal(1, 1);
                client.close();
            });

        });
    });

    it('Missing email', () => {
        const expectedList = [16245];
        csvParserService.readAndPersistCsv(filePath, 'test3').then(result => {
            MongoClient.connect(config.get('mongodb.uri'), function(err, client) {
                const db = client.db(config.get('mongodb.dbName'));
                const collection = db.collection('Patients');
                collection.find({"Email Address": "", "CONSENT": "Y"}).toArray(function(err, docs) {
                    docs.forEach((it, indx) => {
                        assert.equal(it["Member ID"], expectedList[indx]);
                    });
                });
                assert.equal(1, 1);
                client.close();
            });

        });
    });

    it('Emails created', () => {
        emailService.generateEmailTemplates().then(result => {
            MongoClient.connect(config.get('mongodb.uri'), function(err, client) {
                const db = client.db(config.get('mongodb.dbName'));
                const collection = db.collection(config.get('emailing.emailsCollectionName'));
                collection.find({}).toArray(function(err, docs) {
                    let indx = config.get('emailing.generationStartIndx');
                    docs.forEach((it) => {
                        assert.equal(it['id'], indx++ + 1);
                    })
                });
                client.close();
            });
        });
    });

    it('Correct cron', () => {
        // each minute?
        assert.equal(config.get('emailing.schedule'), '* * * * *');
    });

});