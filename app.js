const csvParserService = require('./service/csvParserService');
const emailService = require('./service/emailService');
const readline = require('readline');

const reader = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

reader.question('Please enter .csv file location: \n', (filePath) => {
    csvParserService.readAndPersistCsv(filePath, 'Patients');
    emailService.generateEmailTemplates();
    emailService.scheduleEmailsJob();
    reader.close();
});
